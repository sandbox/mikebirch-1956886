<?php

/**
 * @file
 * Ehive module admin settings.
 */

/**
 * Return the Ehive settings form.
 */
function ehive_admin_settings() {
  $form = array();
  $form['ehive_client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('eHive client ID'),
      '#required' => TRUE,
      '#default_value' => variable_get('ehive_client_id', ''),
      '#description' => t('eHive API Key credentials client ID.')
  );
  $form['ehive_client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('eHive client secret'),
      '#required' => TRUE,
      '#default_value' => variable_get('ehive_client_secret', ''),
      '#description' => t('eHive API Key credentials client secret.')
  );
  $form['ehive_tracking_id'] = array(
      '#type' => 'textfield',
      '#title' => t('eHive tracking ID'),
      '#required' => TRUE,
      '#default_value' => variable_get('ehive_tracking_id', ''),
      '#description' => t('eHive API Key credentials tracking ID.')
  );
  $form['ehive_account_id'] = array(
      '#type' => 'textfield',
      '#title' => t('eHive account ID'),
      '#required' => TRUE,
      '#default_value' => variable_get('ehive_account_id', ''),
      '#description' => t('eHive API Key credentials account ID.')
  );

  return system_settings_form($form);
}
